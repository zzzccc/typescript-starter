module.exports = {
  parser: '@typescript-eslint/parser',
  extends: [
    'airbnb-typescript',
    'plugin:@typescript-eslint/recommended',
    'prettier/@typescript-eslint',
    'plugin:prettier/recommended',
    'plugin:jest/recommended',
    'plugin:jest-formatting/recommended',
  ],
  plugins: ['import', 'jest', 'jest-formatting'],
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  env: {
    jest: true,
  },
  rules: {
    '@typescript-eslint/explicit-member-accessibility': 0,
    '@typescript-eslint/explicit-function-return-type': 0,
    '@typescript-eslint/no-explicit-any': [
      'error',
      {
        ignoreRestArgs: true,
      },
    ],
    'import/prefer-default-export': 'off',
    // No need to enable no-unused-vars in ESLint core since it's covered by typescript-eslint
    'no-unused-vars': 'off',
    // Prefer usage of '@' resolver alias over deeply-nested relative imports
    'no-restricted-modules': ['error', { patterns: ['../../*'] }],
    'no-restricted-imports': ['error', { patterns: ['../../*'] }],
    'class-methods-use-this': 'off',
  },
  settings: {
    // Prevent import/no-extraneous-dependencies from complaining about some core modules
    'import/core-modules': ['aws-lambda', 'dosh_logger', 'jest-extended'],
    'import/parsers': {
      '@typescript-eslint/parser': ['.ts', '.tsx'],
    },
    'import/resolver': {
      // Use <root>/tsconfig.json
      typescript: {},
    },
  },
};
