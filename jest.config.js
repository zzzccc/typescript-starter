module.exports = {
  testPathIgnorePatterns: [
    '/node_modules/',
    '/dist/',
    '/typings/',
    '/acceptance_tests/',
  ],
  testEnvironment: 'node',
  preset: 'ts-jest',
  moduleFileExtensions: ['ts', 'js'],
  testMatch: ['**/*.test.js', '**/*.test.ts'],
  verbose: true,
  globals: {
    'ts-jest': {
      useBabelRc: true,
      diagnostics: {
        warnOnly: true,
      },
    },
  },
  moduleNameMapper: {
    '@/(.*)$': '<rootDir>/src/$1',
  },
};
