export interface Config {
  nodeEnv: string;
  aws: Aws;
}

export interface Aws {
  accountNumber: number;
  region: string;
  kms: Kms;
}

export interface Kms {
  configKey: string;
}
