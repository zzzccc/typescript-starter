# TypeScript Starter
- TypeScript 3.7
- ESLint
  - Airbnb style guide
  - Jest + Jest Formatting
- Prettier
- Jest + Jest Extended

# Usage
Use [degit](https://github.com/Rich-Harris/degit):
```
npx degit gitlab:zzzccc/typescript-starter my-new-typescript-project
```
Then `cd` in and `npm install`.

By default, the generated project scaffolding requires Node 12 LTS or greater - you can alter this in the `engines` section of the `package.json`, or omit the section altogether:
```
  "engines": {
    "npm": "^6.10.0",
    "node": "^12.13.0"
  },
```

Note that an `.nvmrc` is also included:
```
lts/12
```
