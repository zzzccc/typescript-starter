import { add } from '.';

describe('add', () => {
  it('should add', () => {
    expect(add(1, 1)).toBe(2);
  });
});
